package field;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import exceptions.*;
import plants.*;

public class Field {
	public Plant[][] spaces;

	public Field() {
		spaces = new Plant[10][10];
	}

	/**
	 * plantSeed
	 * This method should plant a provided plant in the given
	 * field space. If the field space is not empty, it should
	 * throw a FieldNotTilledException.
	 * @param plant The provided Plant object
	 * @param row The provided row
	 * @param column The provided column
	 * @throws FieldNotTilledException
	 */

	public void plantSeed(Plant plant, int row, int column) throws FieldNotTilledException {

		// Check if the space is empty.
		if (spaces[row][column] == null) {
			// Plant seed.
			spaces[row][column] = plant;
			return;
		}
		else {
			throw new FieldNotTilledException(row, column);
		}


	}

	/**
	 * writeToFile
	 * This method should write the current field out to a file
	 * called 'field.txt'. It should match the format produced by
	 * toString. See the specification for more information.
	 */

	public void writeToFile() {
		PrintWriter outputStream = null;
		try {
			outputStream = new PrintWriter(new FileOutputStream("field.txt"));
		}
		catch (FileNotFoundException e) {
			System.out.println("Error opening the file field.txt.");
			System.exit(0);
		}
		String[] output = toString().split("\n");	// Array of lines need printing.

		// Loop to print each line.
		for (int i = 0; i < output.length; i++) {
			outputStream.println(output[i]);
		}
		outputStream.close();
	}

	/**
	 * toString
	 * This method should produce a string representation of the current field.
	 * See the specification for more information and formatting requirements.
	 * @return A text representation of the field
	 */

	public String toString() {
		String result = "  ";	// Output string.

		// Loop to print the horizontal numbering line.
		for (int i = 0; i < 10; i++) {
			result += i + " ";
		}
		result += "\n";

		// Loop to print the vertical numbering line and plants.
		for (int i = 0; i < 10; i++) {
			result += i + " ";
			for (int j = 0; j < 10; j++) {
				if (spaces[i][j] != null) {
					result += spaces[i][j].symbol + " ";
				}
				else {
					result += ". ";
				}
			}
			result += "\n";
		}
		return result;
	}
}
