package exceptions;

public class NoAvailableSeedlingException extends Exception {
	
	public NoAvailableSeedlingException() { 
		super(); 
	}
	
	public NoAvailableSeedlingException(String message) { 
		super("No available seedlings of type: " + message);
	}
}
