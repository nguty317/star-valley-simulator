package exceptions;

public class FieldNotTilledException extends Exception {
	
	public FieldNotTilledException() { 
		super(); 
	}

	public FieldNotTilledException(int row, int column) { 
		super("Field space [" + row + "," + column + "] not empty."); 
	}
}
