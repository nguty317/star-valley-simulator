package exceptions;

public class BeyondStockException extends Exception {
	
	public BeyondStockException() { 
		super(); 
	}
	
	public BeyondStockException(String message) { 
		super("You have requested more " + message + " than we have in stock."); 
	}
}
