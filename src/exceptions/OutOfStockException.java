package exceptions;

public class OutOfStockException extends Exception {

	public OutOfStockException() { 
		super(); 
	}
	
	public OutOfStockException(String message) { 
		super("You have requested more " + message + " than we have in stock."); 
	}
}
