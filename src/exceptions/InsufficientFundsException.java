package exceptions;

public class InsufficientFundsException extends Exception {
	
	public InsufficientFundsException() { 
		super(); 
	}
	
	public InsufficientFundsException(int payment, int fund) { 
		super("You are trying to pay $" + payment + ", but only have $" + fund); 
	}
}
