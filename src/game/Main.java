package game;

import field.Field;
import plants.*;
import exceptions.*;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {
    private Field field;
    private Scanner input = new Scanner(System.in);
    private static final int HARVEST_DIVISOR = 10;
    private String harvestReceipt = "";
    private Player player = new Player();

    /**
     * sellHarvest
     * This method sells all plants to local buyers, receiving
     * the current buy price for each plant type and adding it to
     * the player's funds. It also generates an itemised receipt,
     * showing the value of each crop. See the specification
     * for more information.
     * @param plants An array of plants to sell
     * @return The total sell price of the harvest
     */

    private int sellHarvest(Plant[] plants) {
        int fl = 0;     // Money made selling flower.
        int fr = 0;     // Money made selling fruit.
        int g = 0;      // Money made selling grain.
        int v = 0;      // Money made selling vegetable.
        int total = 0;  // Total money made.

        // Loop to sell each plant in the harvest list and calculate the amount of money made.
        for (Plant plant : plants) {

            if (plant.symbol == '*') {
                int price = plant.sell();
                total += price;
                fl += price;
            }

            else if (plant.symbol == '@') {
                int price = plant.sell();
                total += price;
                fr += price;
            }

            else if (plant.symbol == '!') {
                int price = plant.sell();
                total += price;
                g += price;
            }

            else if (plant.symbol == '^') {
                int price = plant.sell();
                total += price;
                v += price;
            }
        }

        // Generate harvest receipt.
        harvestReceipt += "Harvest receipt:\n"
                + "Flowers: $" + fl + "\n"
                + "Fruit: $" + fr + "\n"
                + "Grain: $" + g + "\n"
                + "Vegetables: $" + v + "\n";

        // Add income to the player's fund.
        player.addIncome(total);
        return total;
    }

    /**
     * harvestPlants
     * This method checks each field space, and harvests any plants
     * that are at or beyond their maturation date. It should not harvest
     * or till any weeds. See the specification for more information.
     * @return An array of mature Plant objects
     */

    private Plant[] harvestPlants() {

        // ArrayList for harvesting plants.
        ArrayList<Plant> harvest = new ArrayList<>();

        // Loop for harvesting plants.
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {

                // Check if the field space is not null.
                if (field.spaces[i][j] != null) {

                    // Check if the plant is not a weed and is fully matured.
                    if (!field.spaces[i][j].getPlantType().equals("weed") && field.spaces[i][j].isMature()) {

                        // Add plant to harvest ArrayList.
                        harvest.add(field.spaces[i][j]);

                        // Make the space null.
                        field.spaces[i][j] = null;
                    }
                }
            }
        }

        // Convert harvest ArrayList to Array.
        Plant[] harvestList = harvest.toArray(new Plant[harvest.size()]);
        for (int i = 0; i < harvestList.length; i++) {
            harvestList[i] = harvest.get(i);
        }
        return harvestList;
    }

    /**
     * buyPlants
     * This method receives a string of input from the console. It
     * parses this string and buys the number of specific Plant objects.
     * It should add these bought plants to the player's seedlings and
     * deduct the cost from the player's funds. See the specification for
     * more information.
     * @param inputString A string of input in the form "v,3 fl,6"
     */

    private void buyPlants(String inputString) {
        String[] order = inputString.split(" ");    // Array for every orders.
        String[] plant = new String[order.length];         // Array for types of plants that has been ordered.
        int[] num = new int[order.length];                 // Array for number of plants that has been ordered.

        // Loop for buying plants.
        for (int i = 0; i < order.length; i++) {

            String[] subOrder = order[i].split(",");    // Array for components in an order.
            plant[i] = subOrder[0];                            // Type of plants in each order.
            num[i] = Integer.parseInt(subOrder[1]);            // Number of plants in each order.

            try {

                // Check if stock is available.
                if (Nursery.buyPlant(plant[i], num[i], player)) {

                    // Loop to add new plants to seedlings list.
                    for (int j = 0; j < num[i]; j++) {

                        // Temporary plant.
                        Plant newPlant = new Flower();

                        // Assign plant type.
                        switch (plant[i]) {
                            case "fl":
                                newPlant = new Flower();
                                break;
                            case "fr":
                                newPlant = new Fruit();
                                break;
                            case "g":
                                newPlant = new Grain();
                                break;
                            case "v":
                                newPlant = new Vegetable();
                                break;
                        }

                        // Add new plants to seedlings list.
                        player.seedlings.add(newPlant);
                    }
                }

                // Handling exceptions.
            } catch (OutOfStockException | BeyondStockException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * plantPlants
     * This method receives a string of input from the console. It
     * parses this string and plants the specified Plant types in the
     * specified field spaces. It should remove the plants Plant objects
     * from the player's seedlings array. See the specification for
     * more information.
     * @param inputString A string of input in the form "v,2,5 fr,5,4"
     */

    private void plantPlants(String inputString) {
        String[] plantList = inputString.split(" ");    // Array for every requests.
        String[] plant = new String[plantList.length];         // Array for every requested type of plant.
        int[] row = new int[plantList.length];                 // Array for every row.
        int[] column = new int[plantList.length];              // Array for every column.

        // Loop to plant plants.
        for (int i = 0; i < plantList.length; i++) {

            String[] subPlantList = plantList[i].split(",");    // Array for each request components.
            plant[i] = subPlantList[0];                                // Plant type in each request.
            row[i] = Integer.parseInt(subPlantList[1]);                // Row in each request.
            column[i] = Integer.parseInt(subPlantList[2]);             // Column in each request.

            try {

                // Plant seed.
                    field.plantSeed(player.getPlant(plant[i]), row[i], column[i]);

                // Handle exceptions.
            } catch (FieldNotTilledException | NoAvailableSeedlingException e) {
                System.out.println(e.getMessage());
            }
        }
    }


    /**
     * tillSoil
     * This method receives a string of input from the console. It
     * parses this string and tills the soil at the specified field space.
     * The tilling should remove any Plant object in that space, resetting the
     * field space to null. See the specification for more information.
     * @param inputString  A string of input in the form "2,5 5,4"
     */

    private void tillSoil(String inputString) {
        String[] tillList = inputString.split(" ");     // Array for every requests.
        int[] row = new int[tillList.length];                  // Array for every row.
        int[] column = new int[tillList.length];               // Array for every column.

        // Loop to till soil.
        for (int i = 0; i < tillList.length; i++) {

            String[] subTillList = tillList[i].split(",");  // Array for components in each request.
            row[i] = Integer.parseInt(subTillList[0]);             // Row in each request.
            column[i] = Integer.parseInt(subTillList[1]);          // Column in each request.

            // Till soil.
            field.spaces[row[i]][column[i]] = null;
        }
    }

    /**
     * maturePlants
     * This method should check all field spaces for a Plant
     * object and increment its daysPlanted variable by one.
     */

    private void maturePlants() {

        // Loop for each space in the field.
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {

                // Check if the space is null.
                if (field.spaces[i][j] != null) {

                    // Increase days planted.
                    field.spaces[i][j].daysPlanted += 1;
                }
            }

        }
    }

    // ----------- DO NOT MODIFY ANY CODE BELOW ----------- //

    private void runGameLoop() {
        int round = 1;
        String inputString = "";
        while (!isExiting(inputString)) {
            System.out.println(field);
            field.writeToFile();
            if (round % HARVEST_DIVISOR == 0) {
                // Harvest day
                System.out.printf("Round number: %d | Harvest day!\n", round);
                Plant[] plants = harvestPlants();
                int returns = sellHarvest(plants);
                System.out.println(harvestReceipt);
                harvestReceipt = "";
                System.out.printf("Total sales: $%d\n", returns);
                spawnWeeds();
                Nursery.setPrices();
                Nursery.restock();
            }
            else {
                // Regular day, present options
                System.out.printf("Round number: %d | %d rounds until harvest | Funds: $%d\n", round,
                        HARVEST_DIVISOR - round % HARVEST_DIVISOR,
                        player.getFunds());
                inputString = getInputString();

                while (!inputString.equals("e")) {
                    switch (inputString.toLowerCase()) {
                        case "b":
                            // Buy plants from store
                            System.out.println("Welcome to the nursery. What would you like to buy?");
                            System.out.println("We have in stock: " + printStockLevels());
                            System.out.println("Our prices are: " + printPrices());
                            System.out.println("Your available choices are: [fl = Flowers | fr = Fruit | g = Grain | v = Vegetable | l = leave]");
                            System.out.println("Enter in the form 'v,3 fl,6' to buy 3 vegetables and 6 flowers");
                            System.out.print("Choice: ");
                            // Flush the input
                            input.nextLine();
                            // Take input for the purchases
                            String purchases = input.nextLine().trim();
                            buyPlants(purchases);
                            break;
                        case "p":
                            // Plant plants
                            System.out.println("What would you like to plant?");
                            System.out.println("Your available seedlings are: " + printSeedlings());
                            System.out.println("Enter in the form 'v,2,5 fr,5,4' to plant a vegetable in plot row 2, column 5 | l = leave");
                            System.out.print("Choice: ");
                            // Flush the input
                            input.nextLine();
                            // Take input for planting
                            String planting = input.nextLine().trim();
                            plantPlants(planting);
                            break;
                        case "t":
                            // Till soil
                            System.out.println("Which soil plots would you like to till?");
                            System.out.println("Enter in the form '4,7 9,1' to till the soil plots in row 4, column 7 and row 9, column 1 | l = leave");
                            System.out.print("Choice: ");
                            input.nextLine();
                            // Take input for planting
                            String plot = input.nextLine().trim();
                            tillSoil(plot);
                            break;
                        case "q":
                            // Quit game
                            System.exit(0);
                        default:
                            throw new IllegalStateException("Unexpected value: " + inputString.toLowerCase());
                    }
                    inputString = getInputString();
                }
            }
            // End round
            round++;
            System.out.println("Round end");
            maturePlants();
        }
    }

    /*
     * Output the top-level game options to the player and returns their choice.
     */
    private String getInputString() {
        System.out.println("What would you like to do?");
        System.out.println("Your available choices are: [b = Buy Plant | p = Plant plants | t = Till Soil | e = End Round | q = Quit Game]");
        System.out.print("Choice: ");
        return input.next();
    }

    /*
     * Neatly prints the stock level of the nursery.
     */
    private String printStockLevels() {
        return "" +
                "Flowers = " + Nursery.getQuantity("flowers") + " " +
                "Fruit = " + Nursery.getQuantity("fruit") + " " +
                "Grain = " + Nursery.getQuantity("grain") + " " +
                "Vegetables = " + Nursery.getQuantity("vegetables");
    }

    /*
     * Neatly prints the prices of the nursery.
     */
    private String printPrices() {
        return "" +
                "Flowers = $" + Nursery.getPrice("flowers") + " " +
                "Fruit = $" + Nursery.getPrice("fruit") + " " +
                "Grain = $" + Nursery.getPrice("grain") + " " +
                "Vegetables = $" + Nursery.getPrice("vegetables");
    }

    /*
     * Neatly prints the number of each seedling type in the player's seedlings list.
     */
    private String printSeedlings() {
        int[] numberOfSeedlings = new int[4];
        for (Plant p : player.seedlings) {
            if (p instanceof Flower) {
                numberOfSeedlings[0]++;
            }
            else if (p instanceof Fruit) {
                numberOfSeedlings[1]++;
            }
            else if (p instanceof Grain) {
                numberOfSeedlings[2]++;
            }
            else if (p instanceof Vegetable) {
                numberOfSeedlings[3]++;
            }
            else {
                System.out.println("Something went terribly wrong here");
            }
        }
        String output = "" +
                "Flowers = " + numberOfSeedlings[0] + " " +
                "Fruit = " + numberOfSeedlings[1] + " " +
                "Grain = " + numberOfSeedlings[2] + " " +
                "Vegetables = " + numberOfSeedlings[3];
        return output;
    }

    /*
     * Generates the initial field.
     */
    private void populateStartingField() {
        if (field == null) return;

        // Generate weeds
        for (int i = 0; i < field.spaces.length; i++) {
            for (int j = 0; j < field.spaces[i].length; j++) {
                if (new Random().nextInt(100) < 5) {
                    // 5% chance of a weed spawning
                    Plant p = new Weed("Dandelion", '#', 4, 1);
                    field.spaces[i][j] = p;
                }
            }
        }
    }

    /*
     * Called every harvest to spawn some random weeds.
     */
    private void spawnWeeds() {
        // Generate weeds
        for (int i = 0; i < field.spaces.length; i++) {
            for (int j = 0; j < field.spaces[i].length; j++) {
                if (field.spaces[i][j] == null && new Random().nextInt(100) < 25) {
                    // 25% chance of a weed spawning
                    Plant p = new Weed("Dandelion", '#', 4, 1);
                    field.spaces[i][j] = p;
                }
            }
        }
    }

    /*
     * Exits the game.
     */
    private boolean isExiting(String inputString) {
        return inputString.equalsIgnoreCase("q");
    }

    /*
     * Runs the main game loop.
     */
    public static void main(String[] args) {
        Main main = new Main();
        main.field = new Field();
        main.populateStartingField();
        main.runGameLoop();
    }
}
