package game;

import plants.*;

import java.util.ArrayList;

import exceptions.*;

public class Player {
	private int funds;
	public ArrayList<Plant> seedlings = new ArrayList<>();

	public Player() {
		funds = 200;
	}

	/**
	 * getPlant
	 * This method searches the player's seedlings list for a
	 * plant object that matches the type provided. The method must determine
	 * the subclass of each Plant object in the list and return
	 * the first that matches the provided type. It should also remove
	 * it from the seedlings list. If there are no matching seedling
	 * objects of the provided type, it should throw a
	 * NoAvailableSeedlingException. See the specification for more information.
	 * @param type A string representation of the Plant type
	 * @return The first matching plant object from the seedlings list
	 * @throws NoAvailableSeedlingException
	 */

	public Plant getPlant(String type) throws NoAvailableSeedlingException {

		Plant temp = new Flower();	// Temporary plant.

		// Assign plant type.
		switch (type) {
			case "fl":
				temp = new Flower();
				break;
			case "fr":
				temp = new Fruit();
				break;
			case "g":
				temp = new Grain();
				break;
			case "v":
				temp = new Vegetable();
				break;
			default:
				throw new NoAvailableSeedlingException(type);
		}

		// Loop to get plant from seedlings list.
		for (int i = 0; i < seedlings.size(); i++) {

			// Check if the plant belongs to the same class as temp.
			if (seedlings.get(i).getClass() == temp.getClass()) {
				// Assign to temp.
				temp = seedlings.get(i);
				// Remove from seedlings.
				seedlings.remove(i);
				return temp;
			}
		}
		throw new NoAvailableSeedlingException(type);
		//return null;
	}

	/**
	 * pay
	 * This method attempts to subtract a provided amount from the player's
	 * funds. If there is not enough money, it should throw an
	 * InsufficientFundsException. See the specification for more information.
	 * @param payment The amount to be deducted
	 * @throws InsufficientFundsException
	 */

	public void pay(int payment) throws InsufficientFundsException {

		// Check if fund is equal or bigger than paying amount.
		if (funds >= payment) {
			funds = funds - payment;
		}
		else {
			throw new InsufficientFundsException(payment, funds);
		}
	}


	/**
	 * Increments funds by the provided amount.
	 * @param income
	 */
	public void addIncome(int income) {
		funds += income;
	}

	public int getFunds() {
		return funds;
	}
}
