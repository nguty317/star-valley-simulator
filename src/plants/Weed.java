package plants;

public class Weed extends Plant {

	public Weed(String name, char symbol, int maturationTime, int yield) {
		super(name, symbol, maturationTime, yield);
	}

	/**
	 * sell
	 * This method generates a selling price of $0.
	 * @return sell An int for plant's selling price
	 */
	@Override
	public int sell() {
		return 0;
	}

	/**
	 * getPlantType
	 * This method returns a plant's class name in lowercase string.
	 * @return plantType A string that represents class name in lowercase.
	 */
	@Override
	public String getPlantType() {
		return this.getClass().getSimpleName().toLowerCase();
	}

}
