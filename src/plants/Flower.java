package plants;

public class Flower extends Plant {

	private String colour;

	public Flower() {
		super("Daisy", '*', 7, 3);
		this.colour = "";
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	/**
	 * sell
	 * This method generates a random selling price within $7-11.
	 * @return sell An int for plant's selling price
	 */
	@Override
	public int sell() {
		int price = (int)(Math.random()*4) + 7;
		return price * this.yield;
	}

	/**
	 * getPlantType
	 * This method returns a plant's class name in lowercase string.
	 * @return plantType A string that represents class name in lowercase.
	 */
	@Override
	public String getPlantType() {
		return this.getClass().getSimpleName().toLowerCase();
	}

}
