package plants;

public class Grain extends Plant {

	public Grain() {
		super("Wheat", '!', 4, 8);
	}

	/**
	 * sell
	 * This method generates a random selling price within $2-4.
	 * @return sell An int for plant's selling price
	 */
	@Override
	public int sell() {
		int price = (int)(Math.random()*2) + 2;
		return price * this.yield;
	}

	/**
	 * getPlantType
	 * This method returns a plant's class name in lowercase string.
	 * @return plantType A string that represents class name in lowercase.
	 */
	@Override
	public String getPlantType() {
		return this.getClass().getSimpleName().toLowerCase();
	}

}
