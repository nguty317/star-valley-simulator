package plants;

public class Vegetable extends Plant {

	public Vegetable() {
		super("Capsicum", '^', 8, 2);
	}

	/**
	 * sell
	 * This method generates a random selling price within $6-15.
	 * @return sell An int for plant's selling price
	 */
	@Override
	public int sell() {
		int price = (int)(Math.random()*9) + 6;
		return price * this.yield;
	}

	/**
	 * getPlantType
	 * This method returns a plant's class name in lowercase string.
	 * @return plantType A string that represents class name in lowercase.
	 */
	@Override
	public String getPlantType() {
		return this.getClass().getSimpleName().toLowerCase();
	}

}
