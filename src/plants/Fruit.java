package plants;

public class Fruit extends Plant {

	public Fruit() {
		super("Apple", '@', 14, 1);
	}

	/**
	 * sell
	 * This method generates a random selling price within $40-60.
	 * @return sell An int for plant's selling price
	 */
	@Override
	public int sell() {
		int price = (int)(Math.random()*20) + 40;
		return price * this.yield;
	}

	/**
	 * getPlantType
	 * This method returns a plant's class name in lowercase string.
	 * @return plantType A string that represents class name in lowercase.
	 */
	@Override
	public String getPlantType() {
		return this.getClass().getSimpleName().toLowerCase();
	}

}
